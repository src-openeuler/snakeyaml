#Global macro or variable
%global vertag 49e794037c6b

#Basic Information
Name:           snakeyaml
Version:        1.32
Release:        1
Summary:        YAML parser and emitter for the Java programming language
License:        ASL 2.0
URL:            https://bitbucket.org/%{name}/%{name}
Source0:        https://bitbucket.org/%{name}/%{name}/get/%{name}-%{version}.tar.gz
Patch0:         0001-replace-bundled-base64coder-with-java.util.Base64.patch
Patch1:         0002-Replace-bundled-gdata-java-client-classes-with-commo.patch
Patch2:         reader_bom_test_fix.patch
BuildArch:      noarch

#Dependency
BuildRequires:  dos2unix maven-local
BuildRequires:  mvn(commons-codec:commons-codec)
BuildRequires:  mvn(org.apache.velocity:velocity)
BuildRequires:  mvn(junit:junit) mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-source-plugin)

%description
SnakeYAML is a YAML parser and emitter for the Java Virtual Machine.
YAML is a data serialization format designed for human readability
and interaction with scripting languages.

SnakeYAML features:
  * a complete YAML 1.1 parser. In particular,
    SnakeYAML can parse all examples from the specification.
  * Unicode support including UTF-8/UTF-16 input/output.
  * high-level API for serializing and deserializing native Java objects.
  * support for all types from the YAML types repository.
  * relatively sensible error messages.

%package javadoc
Summary:        API documentation for %{name}

%description javadoc
This package contains javadoc for %{name}.

#Build sections
%prep
%autosetup -n %{name}-%{name}-%{vertag} -p1

%mvn_file : %{name}

dos2unix LICENSE.txt

%pom_remove_plugin :cobertura-maven-plugin
%pom_remove_plugin :maven-changes-plugin
%pom_remove_plugin :maven-license-plugin
%pom_remove_plugin :maven-javadoc-plugin
%pom_remove_plugin :maven-site-plugin
%pom_remove_plugin :nexus-staging-maven-plugin

sed -i "/<artifactId>spring</s/spring/&-core/" pom.xml
rm -f src/test/java/examples/SpringTest.java

%pom_add_dep commons-codec:commons-codec

%pom_remove_dep joda-time:joda-time
rm -rf src/test/java/examples/jodatime
%pom_remove_dep org.projectlombok:lombok
%pom_remove_dep org.apache.velocity:velocity-engine-core

rm src/test/java/org/yaml/snakeyaml/issues/issue67/NonAsciiCharsInClassNameTest.java
rm src/test/java/org/yaml/snakeyaml/issues/issue318/ContextClassLoaderTest.java

rm src/test/java/org/yaml/snakeyaml/emitter/template/VelocityTest.java
rm src/test/java/org/yaml/snakeyaml/issues/issue387/YamlExecuteProcessContextTest.java
rm src/test/java/org/yaml/snakeyaml/env/ApplicationProperties.java
rm src/test/java/org/yaml/snakeyaml/env/EnvLombokTest.java
rm src/test/java/org/yaml/snakeyaml/issues/issue527/Fuzzy47047Test.java
rm src/test/java/org/yaml/snakeyaml/issues/issue530/Fuzzy47039Test.java
rm src/test/java/org/yaml/snakeyaml/issues/issue543/Fuzzer50355Test.java
rm src/test/java/org/yaml/snakeyaml/issues/issue525/FuzzyStackOverflowTest.java
rm src/test/java/org/yaml/snakeyaml/issues/issue529/Fuzzy47028Test.java
rm src/test/java/org/yaml/snakeyaml/issues/issue531/Fuzzy47081Test.java
rm src/test/java/org/yaml/snakeyaml/issues/issue526/Fuzzy47027Test.java

rm src/test/resources/issues/issue99.jpeg
rm src/test/resources/reader/unicode-16be.txt
rm src/test/resources/reader/unicode-16le.txt
rm src/test/resources/pyyaml/spec-05-01-utf16be.data
rm src/test/resources/pyyaml/spec-05-01-utf16le.data
rm src/test/resources/pyyaml/spec-05-02-utf16le.data
rm src/test/resources/pyyaml/odd-utf16.stream-error
rm src/test/resources/pyyaml/invalid-character.loader-error
rm src/test/resources/pyyaml/invalid-character.stream-error
rm src/test/resources/pyyaml/invalid-utf8-byte.loader-error
rm src/test/resources/pyyaml/invalid-utf8-byte.stream-error
rm src/test/resources/pyyaml/empty-document-bug.data
rm src/test/resources/pyyaml/spec-05-02-utf16be.data
rm -rf src/test/resources/fuzzer/
rm src/test/java/org/yaml/snakeyaml/issues/issue99/YamlBase64Test.java

%build
%mvn_build

%install
%mvn_install

#Files list
%files -f .mfiles
%license LICENSE.txt

%files javadoc -f .mfiles-javadoc
%license LICENSE.txt

%changelog
* Wed Mar 08 2023 yaoxin <yaoxin30@h-partners.com> - 1.32-1
- Update to 1.32 for fix CVE-2022-41854,CVE-2022-25857 and CVE-2022-38749-to-CVE-2022-38752

* Tue Feb 11 2020 Jiangping Hu <hujp1985@foxmail.com> - 1.17-8
- Package init

